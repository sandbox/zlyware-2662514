This module allows the access the Microsoft Translator API.

Usage:

Follow the instructions on 
http://blogs.msdn.com/b/translation/p/gettingstarted1.aspx
for getting your Client ID and Client Secret keys, then submit these 
keys on the module's administration page at 
admin/config/services/microsoft_translator_api

If the keys are valid, you can use translation services as the follow:

microsoft_translator_api_languages()
This call returns an array with the possible language codes.

microsoft_translator_api_detect('Submit')
This call returns the code of the detected language (en)

microsoft_translator_api_translate('Submit', 'en', 'hu')
This call returns the translated text from English to Hungarian.

There is a last, "force" parameter for these functions, and when
that is true, then the usage permission check skipped.

You can translate 2 million characters per month for free.

Enjoy.
