<?php
/**
 * @file
 *   RSS Data Miner administration functions.
 */

/**
 * RSS Data Miner configuration form.
 */
function rss_data_miner_admin_settings($form, &$form_state) {
  $form = array();
  
  $form['rss_data_miner_keyword'] = array(
    '#type' => 'textfield',
    '#title' => t('The RSS Data Miner Keyword'),
    '#required' => TRUE,
    '#default_value' => variable_get('rss_data_miner_keyword', ''),
    '#description' => "Please enter the keyword to search for.",
    '#size' => 50,
    '#maxlength' => 50,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save configuration',
  );

  return $form;
}

/**
 * RSS Data Miner submit.
 */
function rss_data_miner_admin_settings_submit($form, &$form_state) {
  variable_set('rss_data_miner_keyword', strtolower($form_state['values']['rss_data_miner_keyword']));
  _rss_data_miner_init();
}
