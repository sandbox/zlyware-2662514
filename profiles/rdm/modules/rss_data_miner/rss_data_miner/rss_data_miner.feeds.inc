<?php
/**
 * @file
 *   RSS Data Miner utility functions.
 */

/**
 * Implements hook_feeds_plugins().
 */
function rss_data_miner_feeds_plugins() {
  $info = array();
  $info['RssDataMinerProcessor'] = array(
    'name' => 'RSS Data Miner processor',
    'description' => 'Process the data for RSS Data Miner.',
    'handler' => array(
      'parent' => 'FeedsProcessor', // A plugin needs to derive either directly or indirectly from FeedsFetcher, FeedsParser or FeedsProcessor.
      'class' => 'RssDataMinerProcessor',
      'file' => 'rss_data_miner.feeds.inc',
      'path' => drupal_get_path('module', 'rss_data_miner'),
    ),
  );
  return $info; 
}

/**
 * Allows a feed node to populate fields.
 */
class RssDataMinerProcessor extends FeedsProcessor {

  /**
   * Define entity type.
   */
  public function entityType() {
    return 'node';
  } 
  
  /**
   * Creates a new node in memory and returns it.
   */
  protected function newEntity(FeedsSource $source) {
    $node = new stdClass();
    $node->type = 'forum';
    $node->changed = REQUEST_TIME;
    $node->created = REQUEST_TIME;
    $node->language = LANGUAGE_NONE;
    $node->is_new = TRUE;
    node_object_prepare($node);
    $node->log = 'Created by RssDataMinerProcessor.';
    $node->uid =  drupal_anonymous_user()->uid;
    return $node;
  } 
  
  /**
   * Save a node.
   */
  public function entitySave($entity) {
    node_save($entity);
  }

  /**
   * Delete a series of nodes.
   */
  protected function entityDeleteMultiple($nids) {
    node_delete_multiple($nids);
  }
  
  /**
   * Process the result of the parsing stage.
   *
   * @param FeedsSource $source
   *   Source information about this import.
   * @param FeedsParserResult $parser_result
   *   The result of the parsing stage.
   */
  public function process(FeedsSource $source, FeedsParserResult $parser_result) {
    $state = $source->state(FEEDS_PROCESS);
    if (!isset($state->removeList) && $parser_result->items) {
      $this->initEntitiesToBeRemoved($source, $state);
    }

    while ($item = $parser_result->shiftItem()) {

      // Check if this item already exists.
      $entity_id = $this->existingEntityId($source, $parser_result);
      // If it's included in the feed, it must not be removed on clean.
      if ($entity_id) {
        unset($state->removeList[$entity_id]);
      }

      module_invoke_all('feeds_before_update', $source, $item, $entity_id);

      // If it exists pass onto the next item.
      if ($entity_id) {
        continue;
      }

      try {

        $hash = $this->hash($item);
        $entity = $this->newEntity($source);
        $this->newItemInfo($entity, $source->feed_nid, $hash);

        // Set property and field values.
        $this->mapping($source, $parser_result, $entity, $item);
        $this->entityValidate($entity);

        // Allow modules to alter the entity before saving.
        module_invoke_all('feeds_presave', $source, $entity, $item, $entity_id);
        if (module_exists('rules')) {
          rules_invoke_event('feeds_import_'. $source->importer()->id, $entity);
        }

        // Enable modules to skip saving at all.
        if (!empty($entity->feeds_item->skip)) {
          continue;
        }

        // This will throw an exception on failure.
        $this->entitySaveAccess($entity);
        $this->entitySave($entity);

        // Allow modules to perform operations using the saved entity data.
        // $entity contains the updated entity after saving.
        module_invoke_all('feeds_after_save', $source, $entity, $item, $entity_id);
        $state->created++;
      }

      // Something bad happened, log it.
      catch (Exception $e) {
        $state->failed++;
        drupal_set_message($e->getMessage(), 'warning');
        list($message, $arguments) = $this->createLogEntry($e, $entity, $item);
        $source->log('import', $message, $arguments, WATCHDOG_ERROR);
      }
    }

    // Set messages if we're done.
    if ($source->progressImporting() != FEEDS_BATCH_COMPLETE) {
      return;
    }
    // Remove not included items if needed.
    // It depends on the implementation of the clean() method what will happen
    // to items that were no longer in the source.
    $this->clean($state);
    $info = $this->entityInfo();
    $tokens = array(
      '@entity' => strtolower($info['label']),
      '@entities' => strtolower($info['label plural']),
    );
    $messages = array();
    if ($state->created) {
      $messages[] = array(
       'message' => format_plural(
          $state->created,
          'Created @number @entity.',
          'Created @number @entities.',
          array('@number' => $state->created) + $tokens
        ),
      );
    }
    if ($state->updated) {
      $messages[] = array(
       'message' => format_plural(
          $state->updated,
          'Updated @number @entity.',
          'Updated @number @entities.',
          array('@number' => $state->updated) + $tokens
        ),
      );
    }
    if ($state->unpublished) {
      $messages[] = array(
        'message' => format_plural(
            $state->unpublished,
            'Unpublished @number @entity.',
            'Unpublished @number @entities.',
            array('@number' => $state->unpublished) + $tokens
        ),
      );
    }
    if ($state->blocked) {
      $messages[] = array(
        'message' => format_plural(
          $state->blocked,
          'Blocked @number @entity.',
          'Blocked @number @entities.',
          array('@number' => $state->blocked) + $tokens
        ),
      );
    }
    if ($state->deleted) {
      $messages[] = array(
       'message' => format_plural(
          $state->deleted,
          'Removed @number @entity.',
          'Removed @number @entities.',
          array('@number' => $state->deleted) + $tokens
        ),
      );
    }
    if ($state->failed) {
      $messages[] = array(
       'message' => format_plural(
          $state->failed,
          'Failed importing @number @entity.',
          'Failed importing @number @entities.',
          array('@number' => $state->failed) + $tokens
        ),
        'level' => WATCHDOG_ERROR,
      );
    }
    if (empty($messages)) {
      $messages[] = array(
        'message' => t('There are no new @entities.', array('@entities' => strtolower($info['label plural']))),
      );
    }
    foreach ($messages as $message) {
      drupal_set_message($message['message']);
      $source->log('import', $message['message'], array(), isset($message['level']) ? $message['level'] : WATCHDOG_INFO);
    }
  }
  
  /**
   * Overrides parent::hasConfigForm().
   */
  public function hasConfigForm() {
    return FALSE;
  }

  /**
   * Implements parent::entityInfo().
   */
  protected function entityInfo() {
    $info = parent::entityInfo();
    $info['label plural'] = t('Nodes');
    return $info;
  }
 
  public function mapping(FeedsSource $source, FeedsParserResult $parser_result, &$node, $item) {
    
    /**
     * Set the node title.
     *
     * - Remove all tags from the title.
     */
    $node->title = strip_tags($item['title']);

    /**
     * Set the body field.
     *
     * OpenCalais will process this "body" field. So, we try to download 
     * the full content for better analysis. It the language of the incoiming
     * content is not supported by OpenCalais, then we translate it. We hide
     * the content of this field, it is never displayed.
     */
    $fullBody = '';
    if ($fullDocument = $this->getFullDocument(substr($item['url'], strrpos($item['url'], 'http')))) {
      $html_obj = new simple_html_dom();
      $html_obj->load($fullDocument);
      foreach ($html_obj->find('body') as $body) {
        $fullBody = $body;
      }
    }
    if ($fullBody == '') {
      $fullBody = $item['description'];
    }
    $node->body[LANGUAGE_NONE][0] = array(
      'value' => $this->openCalaisTranslate($item['description']),
      'format' => 'filtered_html',
    );

    /**
     * Set the original description.
     *
     * We will display the original content instead of body.
     */
    $node->field_original_body[LANGUAGE_NONE][0] = array(
      'value' => $item['description'],
      'format' => 'filtered_html',
    );

    /**
     * Set the forum taxonomy.
     *
     * We try to download the index page of the content (http://hostname), 
     * and extract the title of the website. If it fails, then the taxonomy 
     * term will the hostname.
     */
    $node->taxonomy_forums[LANGUAGE_NONE][0]['tid'] = $this->setTaxonomy($item['url']);
    
    /**
     * Set the link to the original content.
     *
     * We remove the redirection from the link in these cases:
     * http://hostname/?q=http://hostname/linked_content
     */
    $node->field_link[LANGUAGE_NONE][0] = array(
      'url' => substr($item['url'], strrpos($item['url'], 'http')),
      'title' => $node->title,
    );
        
    /**
     * Set the parent feed of the content for wiews.
     */
    $node->field_feed[LANGUAGE_NONE][0]['nid'] = $node->feeds_item->feed_nid;
    
    $node->feeds_item->url = $item['url'];
    $node->feeds_item->guid = $item['guid'];
    dsm($node);
  }
  
  /**
   * Translate text if the language not supported.
   *
   * We detect the language of the parameter, and if OpenCalais
   * does not support it, then translate it to English.
   */
  private function openCalaisTranslate($text) {
    if ($language = microsoft_translator_api_detect($text, TRUE)) {
      if (!in_array($language, opencalais_api_supported_languages())) {
        $text = microsoft_translator_api_translate($text, $language, opencalais_api_supported_languages()[0], TRUE);
      }
    }
    return $text;
  }

  /**
   * Download an URL.
   *
   * We try to download the url.
   * When something bad happened, then return an empty string.
   */
  public function getFullDocument($url) {
    $request = drupal_http_request($url);
    return $request->code == '200' ? $request->data : '';
  }
  
  /**
   * Create or get a taxonomy term for the forum topic.
   *
   * We try to download the index page of the content (http://hostname), 
   * and extract the title of the website. If it fails, then the taxonomy 
   * term will the hostname. If the term does not exists, then it will 
   * created. The return value is the tid of the taxonomy term.
   */
  private function setTaxonomy($url) {
    $source = parse_url($url, PHP_URL_SCHEME) . '://' . parse_url($url, PHP_URL_HOST);
    if ($data = $this->getFullDocument($source)) {
      $html_obj = new simple_html_dom();
      $html_obj->load($data);
      foreach ($html_obj->find('title') as $title) {
        $source = strip_tags($title);
      }
    } else {
      $source = parse_url($url, PHP_URL_HOST);
    }
    $term = taxonomy_get_term_by_name($source, 'forums');
    if (count($term) == 0) {
      $term = new stdClass();
      $term->name = $source;
      $term->vid = taxonomy_vocabulary_machine_name_load('forums')->vid;
      taxonomy_term_save($term);
    } else {
      $term = $term[key($term)];
    }
    return $term->tid;
  }

}
