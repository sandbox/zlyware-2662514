<?php
/**
 * @file
 *   RSS Data Miner select functions.
 */

/**
 * Returns all node's nid in the specified content type.
 */
function _rss_data_miner_select_all_node_nid_by_type($type) {
  return db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('type', $type)
    ->condition('status', NODE_PUBLISHED)
    ->execute()
    ->fetchAllAssoc('nid');
}

/**
 * Returns the feeds_item's feed_nid by source url.
 */
function _rss_data_miner_select_feeds_nid_by_source($source) {
  return db_select('feeds_source', 'f')
    ->fields('f', array('feed_nid'))
    ->condition('source', $source)
    ->execute()
    ->fetchAllAssoc('feed_nid');
}

/**
 * Returns the engine's id by url.
 */
function _rss_data_miner_select_engines_by_url($url) {
  return db_select('field_data_field_url', 'f')
    ->fields('f', array('entity_id'))
    ->condition('field_url_value', $url)
    ->execute()
    ->fetchAllAssoc('entity_id');
}
