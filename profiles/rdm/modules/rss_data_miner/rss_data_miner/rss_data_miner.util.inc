<?php
/**
 * @file
 *   RSS Data Miner utility functions.
 */

/**
 * Initialize the module.
 */
function _rss_data_miner_init() {
  watchdog('rss_data_miner_init', 'The RSS Data Miner module initialized called for @keyword.', array('@keyword' => variable_get('rss_data_miner_keyword', '')));
  variable_del('rss_data_miner_mta_languages');
  variable_del('rss_data_miner_keyword_language');
  if ($keyword = variable_get('rss_data_miner_keyword', '')) {
    _rss_data_miner_create_engines();
    variable_set('site_name', ucfirst($keyword) . ' data miner');
    variable_set('site_slogan', 'We found everything about ' . $keyword . '...');
    cache_clear_all();
    watchdog('rss_data_miner_init', 'The RSS Data Miner module initialized successfully for @keyword.', array('@keyword' => $keyword));
  }
}

/**
 * Detect the keyword language.
 */
function _rss_data_miner_get_keyword_language() {
  $keyword_language = '';
  if ($keyword = variable_get('rss_data_miner_keyword', '')) {
    $keyword_language = variable_get('rss_data_miner_keyword_language', '');
    if ($keyword_language == '') {
      $keyword_language = microsoft_translator_api_detect($keyword, TRUE);
      if ($keyword_language) {
        variable_set('rss_data_miner_keyword_language', $keyword_language);
        watchdog('rss_data_miner_keyword_language', 'Keyword language saved: @keyword_language.', array('@keyword_language' => $keyword_language));
      }
    }
  }
  return $keyword_language;
}

/**
 * Detect the common languages between Drupal and Microsoft Translator API.
 */
function _rss_data_miner_get_languages() {
  $languages = variable_get('rss_data_miner_mta_languages', array());
  if (count($languages) == 0) {
    if ($keyword = variable_get('rss_data_miner_keyword', '')) {
      if ($keyword_language = _rss_data_miner_get_keyword_language()) {
        if (is_array($mta_languages = microsoft_translator_api_languages(TRUE))) {
          $drupal_languages = _locale_get_predefined_list();
          foreach ($mta_languages as $mta_index => $mta_langcode) {
            if (array_key_exists($mta_langcode, $drupal_languages)) {
              $languages[$mta_langcode]['name'] = $drupal_languages[$mta_langcode][0];
              if ($keyword_language == $mta_langcode) {
                $languages[$mta_langcode]['keyword'] = $keyword;
              } 
              else {
                if ($translated_keyword = strtolower(microsoft_translator_api_translate($keyword, $keyword_language, $mta_langcode, TRUE))) {
                  $languages[$mta_langcode]['keyword'] = $translated_keyword;
                }
                else {
                  $languages = array();
                  break;
                }
              }
            }
          }
          variable_set('rss_data_miner_mta_languages', $languages);
          watchdog('rss_data_miner_mta_languages', '@count languages are saved.', array('@count' => count($languages)));
        }
      }
    }
  }
  return $languages;
}

/**
 * Create engines from the .info file.
 */
function _rss_data_miner_create_engines() {
  $info = drupal_parse_info_file(drupal_get_path('module', 'rss_data_miner') . '/rss_data_miner.info');
  foreach ($info['engines'] as $url) {
    _rss_data_miner_create_engine($url, user_load(0));
  }
}

/**
 * Create an engine node, if it does not exists.
 */
function _rss_data_miner_create_engine($url, $user) {
  if (count(_rss_data_miner_select_engines_by_url($url)) == 0) {
    module_load_include('inc', 'node', 'node.pages');

    // Prepare node object.
    $node = (object) array(
      'uid' => $user->uid,
      'name' => $user->name,
      'type' => 'engine',
      'language' => LANGUAGE_NONE,
      'status' => NODE_PUBLISHED,
    );
    node_object_prepare($node);

    // Get default values from attached fields.
    $fields = field_info_instances('node');
    $form_state = array();
    foreach($fields['engine'] as $field_name => $values) {
      $form_state['values'][$field_name] = array(LANGUAGE_NONE => array());
    }

    // Add the $form_state field values.
    $form_state['values']['field_url'][LANGUAGE_NONE][0]['value'] = $url;

    // Without this line, not quite sure why, it wont work.
    $form_state['values']['op'] = t('Save');

    // Submit the form.
    drupal_form_submit('engine_node_form', $form_state, $node);
    
    // Tell watch dog if any of the fields fail validation.
    $errors = form_get_errors();
    if (!empty($errors)) {
      foreach ($errors as $field_name => $message) {
        watchdog('rss_data_miner_create_engine', '%field: %message', array('%message'=> $message, '%field' => $field_name), WATCHDOG_ERROR);
      }
    }    
  }
}

/**
 * Check engine URL.
 */
function _rss_data_miner_check_engine($form, &$form_state) {
  $request = drupal_http_request(str_replace('%l', 'en', str_replace('%q', variable_get('rss_data_miner_keyword'), $form_state['values']['field_url'][LANGUAGE_NONE][0]['value'])));
  if ($request->code == 200) {
    feeds_include_library('common_syndication_parser.inc', 'common_syndication_parser');
    if ($data = common_syndication_parser_parse($request->data)) {
      if (count($data['items'])) {
        $title = str_replace('&quot;', '"', $data['title']);
        form_set_value($form['title'], $title, $form_state);
        form_set_value($form['body'][LANGUAGE_NONE][0]['value'], $data['description'], $form_state);
        form_set_value($form['field_original_body'][LANGUAGE_NONE][0]['value'], serialize($request->data), $form_state);
        form_set_value($form['field_image'][LANGUAGE_NONE][0], _rss_data_miner_get_image($request->data), $form_state);
dsm($form);
dsm($form_state);
        if (count(form_get_errors() == 0)) {
          watchdog('rss_data_miner_check_engine', 'A new search engine is successfully created: @title', array('@title' => $title));
        }
      }
      else {
        form_set_error('field_url', t('This feed does not contains any items.'));
        watchdog('rss_data_miner_check_engine', 'This feed does not contains any items.', array(), WATCHDOG_ERROR);
      }
    }
    else {
      form_set_error('field_url', t('The parsing of the content of this feed was failed.'));
      watchdog('rss_data_miner_check_engine', 'The parsing of the content of this feed was failed.', array(), WATCHDOG_ERROR);
    }
  }
  else {
    form_set_error('field_url', t('The status code of the request was @code.', array('@code' => $request->code)));
    watchdog('rss_data_miner_check_engine', 'The status code of the request was @code.', array('@code' => $request->code), WATCHDOG_ERROR);
  }
}

/**
 * Create RSS feeds by URL.
 */
function _rss_data_miner_get_image($rawdata) {

  @$xml = simplexml_load_string($rawdata, NULL, LIBXML_NOERROR | LIBXML_NOWARNING | LIBXML_NOCDATA);
  
  // Got a malformed XML.
  if ($xml === FALSE || is_null($xml)) {
    return FALSE;
  } 
  elseif (property_exists($xml, 'channel') && property_exists($xml->channel, 'image')) {
    $request = drupal_http_request($xml->channel->image->url);
    if ($request->code == 200) {
      $directory = 'public://source_engine';
      if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
        $filename = parse_url($xml->channel->image->url, PHP_URL_PATH);
        $fullpath = $directory . substr($filename, strrpos($filename, '/'));
        if ($file = file_save_data($request->data, $fullpath, FILE_EXISTS_RENAME)) {
          return array(
            'fid' => $file->fid,
            'display' => 1,
            'title' => $xml->channel->image->title,
          );
        }
      }
    }
  }
}

/**
 * Create RSS feeds by URL.
 */
function _rss_data_miner_create_feeds() {
  $engines = _rss_data_miner_select_all_node_nid_by_type('engine');
  foreach ($engines as $engine) {
    $node_engine = node_load($engine->nid);
    $link = $node_engine->field_url[LANGUAGE_NONE][0]['value'];
    foreach (_rss_data_miner_get_languages() as $langcode => $definition) {
      _rss_data_miner_create_feed(str_replace('%l', $langcode, str_replace('%q', $definition['keyword'], $link)), $engine->nid);
    }
  }
}

/**
 * Create a RSS feed node, if it does not exists.
 */
function _rss_data_miner_create_feed($source, $engine) {
  $request = drupal_http_request($source);
  if ($request->code == '200') {
    if (count(_rss_data_miner_select_feeds_nid_by_source($source)) == 0) {
      $node = new stdClass();
      $node->type = 'rss_feed';
      $node->title = parse_url($source, PHP_URL_HOST);
      node_object_prepare($node);
      $node->language = LANGUAGE_NONE;
      $node->status = NODE_PUBLISHED;
      $node->feeds['FeedsHTTPFetcher']['source'] = $source;
      $node->field_search_engine[LANGUAGE_NONE][0]['nid'] = $engine;
      node_save($node);
      watchdog('rss_data_miner_create_feed', 'A new RSS feed is successfully created: !source', array('!source' => $source));
    }
  }
}
