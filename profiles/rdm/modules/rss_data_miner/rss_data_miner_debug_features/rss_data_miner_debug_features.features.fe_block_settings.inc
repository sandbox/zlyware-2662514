<?php
/**
 * @file
 * rss_data_miner_debug_features.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function rss_data_miner_debug_features_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['devel-execute_php'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'execute_php',
    'module' => 'devel',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'lexus_zymphonies_theme' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'lexus_zymphonies_theme',
        'weight' => -11,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['devel-switch_user'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'switch_user',
    'module' => 'devel',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'lexus_zymphonies_theme' => array(
        'region' => 'footer_first',
        'status' => 1,
        'theme' => 'lexus_zymphonies_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
