<?php
/**
 * @file
 * rss_data_miner_debug_features.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function rss_data_miner_debug_features_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'switch users'.
  $permissions['switch users'] = array(
    'name' => 'switch users',
    'roles' => array(
      'administrator user' => 'administrator user',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'devel',
  );

  return $permissions;
}
