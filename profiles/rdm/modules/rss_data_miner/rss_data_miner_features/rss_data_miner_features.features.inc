<?php
/**
 * @file
 * rss_data_miner_features.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function rss_data_miner_features_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "backup_migrate" && $api == "backup_migrate_exportables") {
    return array("version" => "1");
  }
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function rss_data_miner_features_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function rss_data_miner_features_flag_default_flags() {
  $flags = array();
  // Exported flag: "Bookmark".
  $flags['bookmark'] = array(
    'entity_type' => 'node',
    'title' => 'Bookmark',
    'global' => 0,
    'types' => array(
      0 => 'forum',
      1 => 'rss_feed',
      2 => 'engine',
    ),
    'flag_short' => 'Bookmark this',
    'flag_long' => 'Add this post to your bookmarks.',
    'flag_message' => 'This post has been added to your bookmarks.',
    'unflag_short' => 'Unbookmark this',
    'unflag_long' => 'Remove this post from your bookmarks.',
    'unflag_message' => 'This post has been removed from your bookmarks.',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 0,
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'module' => 'rss_data_miner_features',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  // Exported flag: "Irrelevant".
  $flags['irrelevant'] = array(
    'entity_type' => 'node',
    'title' => 'Irrelevant',
    'global' => 1,
    'types' => array(
      0 => 'forum',
      1 => 'rss_feed',
      2 => 'engine',
    ),
    'flag_short' => 'Irrelevant content',
    'flag_long' => 'Mark this content as irrelevant.',
    'flag_message' => 'This content marked as irrelevant.',
    'unflag_short' => 'Relevant content',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => 'This content marked as irrelevant.',
    'link_type' => 'confirm',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 0,
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'flag_confirmation' => 'Are you sure to mark this content as irrelevant?',
    'unflag_confirmation' => '',
    'module' => 'rss_data_miner_features',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}

/**
 * Implements hook_node_info().
 */
function rss_data_miner_features_node_info() {
  $items = array(
    'engine' => array(
      'name' => t('Search engine'),
      'base' => 'node_content',
      'description' => t('The <em>search engine</em> stores the different URL addresses of the internet search engines.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('The search engine URL must be a string that contains the value "%q", and %q will be replaced with the site specified keyword. The site specified keyword in all available languages will be replaced. For example the URL of the Google News RSS channel is: https://news.google.com/news?hl=%l&ned=us&ie=UTF-8&q="%q"&nolr=1&output=rss'),
    ),
    'rss_feed' => array(
      'name' => t('RSS feed'),
      'base' => 'node_content',
      'description' => t('A <em>RSS feed</em> content type is used by the Feeds module.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
