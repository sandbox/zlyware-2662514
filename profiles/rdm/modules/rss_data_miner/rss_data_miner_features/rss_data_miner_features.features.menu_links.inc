<?php
/**
 * @file
 * rss_data_miner_features.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function rss_data_miner_features_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_forums:forum.
  $menu_links['main-menu_forums:forum'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'forum',
    'router_path' => 'forum',
    'link_title' => 'Forums',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_forums:forum',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_home:<front>.
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_search-engines:engine.
  $menu_links['main-menu_search-engines:engine'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'engine',
    'router_path' => 'engine',
    'link_title' => 'Search engines',
    'options' => array(
      'attributes' => array(
        'title' => 'Search engines used by this page.',
      ),
      'identifier' => 'main-menu_search-engines:engine',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Forums');
  t('Home');
  t('Search engines');

  return $menu_links;
}
