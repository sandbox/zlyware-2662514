<?php
/**
 * @file
 * rss_data_miner_features.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function rss_data_miner_features_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access comments'.
  $permissions['access comments'] = array(
    'name' => 'access comments',
    'roles' => array(
      'administrator user' => 'administrator user',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'comment',
  );

  // Exported permission: 'access content'.
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      'administrator user' => 'administrator user',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'access content overview'.
  $permissions['access content overview'] = array(
    'name' => 'access content overview',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'administer content types'.
  $permissions['administer content types'] = array(
    'name' => 'administer content types',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'administer nodes'.
  $permissions['administer nodes'] = array(
    'name' => 'administer nodes',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'bypass node access'.
  $permissions['bypass node access'] = array(
    'name' => 'bypass node access',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create engine content'.
  $permissions['create engine content'] = array(
    'name' => 'create engine content',
    'roles' => array(
      'administrator user' => 'administrator user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create forum content'.
  $permissions['create forum content'] = array(
    'name' => 'create forum content',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create rss_feed content'.
  $permissions['create rss_feed content'] = array(
    'name' => 'create rss_feed content',
    'roles' => array(
      'administrator user' => 'administrator user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any engine content'.
  $permissions['delete any engine content'] = array(
    'name' => 'delete any engine content',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any forum content'.
  $permissions['delete any forum content'] = array(
    'name' => 'delete any forum content',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any rss_feed content'.
  $permissions['delete any rss_feed content'] = array(
    'name' => 'delete any rss_feed content',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own engine content'.
  $permissions['delete own engine content'] = array(
    'name' => 'delete own engine content',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own forum content'.
  $permissions['delete own forum content'] = array(
    'name' => 'delete own forum content',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own rss_feed content'.
  $permissions['delete own rss_feed content'] = array(
    'name' => 'delete own rss_feed content',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete revisions'.
  $permissions['delete revisions'] = array(
    'name' => 'delete revisions',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any engine content'.
  $permissions['edit any engine content'] = array(
    'name' => 'edit any engine content',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any forum content'.
  $permissions['edit any forum content'] = array(
    'name' => 'edit any forum content',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any rss_feed content'.
  $permissions['edit any rss_feed content'] = array(
    'name' => 'edit any rss_feed content',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own engine content'.
  $permissions['edit own engine content'] = array(
    'name' => 'edit own engine content',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own forum content'.
  $permissions['edit own forum content'] = array(
    'name' => 'edit own forum content',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own rss_feed content'.
  $permissions['edit own rss_feed content'] = array(
    'name' => 'edit own rss_feed content',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'flag bookmark'.
  $permissions['flag bookmark'] = array(
    'name' => 'flag bookmark',
    'roles' => array(
      'administrator user' => 'administrator user',
      'authenticated user' => 'authenticated user',
      'editor user' => 'editor user',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'flag irrelevant'.
  $permissions['flag irrelevant'] = array(
    'name' => 'flag irrelevant',
    'roles' => array(
      'administrator user' => 'administrator user',
      'authenticated user' => 'authenticated user',
      'editor user' => 'editor user',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'post comments'.
  $permissions['post comments'] = array(
    'name' => 'post comments',
    'roles' => array(
      'administrator user' => 'administrator user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'comment',
  );

  // Exported permission: 'revert revisions'.
  $permissions['revert revisions'] = array(
    'name' => 'revert revisions',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'search content'.
  $permissions['search content'] = array(
    'name' => 'search content',
    'roles' => array(
      'administrator user' => 'administrator user',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search',
  );

  // Exported permission: 'skip comment approval'.
  $permissions['skip comment approval'] = array(
    'name' => 'skip comment approval',
    'roles' => array(
      'administrator user' => 'administrator user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'comment',
  );

  // Exported permission: 'unflag bookmark'.
  $permissions['unflag bookmark'] = array(
    'name' => 'unflag bookmark',
    'roles' => array(
      'administrator user' => 'administrator user',
      'authenticated user' => 'authenticated user',
      'editor user' => 'editor user',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'unflag irrelevant'.
  $permissions['unflag irrelevant'] = array(
    'name' => 'unflag irrelevant',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'view own unpublished content'.
  $permissions['view own unpublished content'] = array(
    'name' => 'view own unpublished content',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'view revisions'.
  $permissions['view revisions'] = array(
    'name' => 'view revisions',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'node',
  );

  return $permissions;
}
