<?php
/**
 * @file
 * rss_data_miner_features.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function rss_data_miner_features_user_default_roles() {
  $roles = array();

  // Exported role: administrator user.
  $roles['administrator user'] = array(
    'name' => 'administrator user',
    'weight' => 2,
  );

  // Exported role: editor user.
  $roles['editor user'] = array(
    'name' => 'editor user',
    'weight' => 3,
  );

  return $roles;
}
