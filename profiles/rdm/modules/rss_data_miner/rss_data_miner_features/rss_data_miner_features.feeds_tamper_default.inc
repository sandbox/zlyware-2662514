<?php
/**
 * @file
 * rss_data_miner_features.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function rss_data_miner_features_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_data_miner-blank_source_1-php';
  $feeds_tamper->importer = 'rss_data_miner';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'return _rss_data_miner_set_forum_taxonomy(item[\'url\']);',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set forum taxonomy term';
  $export['rss_data_miner-blank_source_1-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_data_miner-description-php';
  $feeds_tamper->importer = 'rss_data_miner';
  $feeds_tamper->source = 'description';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'return _rss_data_miner_process_text($field);',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Process text';
  $export['rss_data_miner-description-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_data_miner-description-required';
  $feeds_tamper->importer = 'rss_data_miner';
  $feeds_tamper->source = 'description';
  $feeds_tamper->plugin_id = 'required';
  $feeds_tamper->settings = array(
    'invert' => 0,
    'log' => 1,
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Required field';
  $export['rss_data_miner-description-required'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_data_miner-title-php';
  $feeds_tamper->importer = 'rss_data_miner';
  $feeds_tamper->source = 'title';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'return _rss_data_miner_process_text($field);',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Process text';
  $export['rss_data_miner-title-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_data_miner-title-required';
  $feeds_tamper->importer = 'rss_data_miner';
  $feeds_tamper->source = 'title';
  $feeds_tamper->plugin_id = 'required';
  $feeds_tamper->settings = array(
    'invert' => 0,
    'log' => 1,
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Required field';
  $export['rss_data_miner-title-required'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_data_miner-title-strip_tags';
  $feeds_tamper->importer = 'rss_data_miner';
  $feeds_tamper->source = 'title';
  $feeds_tamper->plugin_id = 'strip_tags';
  $feeds_tamper->settings = array(
    'allowed_tags' => '',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Strip tags';
  $export['rss_data_miner-title-strip_tags'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_data_miner-url-php';
  $feeds_tamper->importer = 'rss_data_miner';
  $feeds_tamper->source = 'url';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'return substr($field, strrpos($field, \'http\'));',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Remove redirection';
  $export['rss_data_miner-url-php'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_data_miner-url-required';
  $feeds_tamper->importer = 'rss_data_miner';
  $feeds_tamper->source = 'url';
  $feeds_tamper->plugin_id = 'required';
  $feeds_tamper->settings = array(
    'invert' => 0,
    'log' => 1,
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Required field';
  $export['rss_data_miner-url-required'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_data_miner-url-unique';
  $feeds_tamper->importer = 'rss_data_miner';
  $feeds_tamper->source = 'url';
  $feeds_tamper->plugin_id = 'unique';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Unique';
  $export['rss_data_miner-url-unique'] = $feeds_tamper;

  return $export;
}
