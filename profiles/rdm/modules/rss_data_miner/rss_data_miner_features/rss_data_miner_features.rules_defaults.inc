<?php
/**
 * @file
 * rss_data_miner_features.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function rss_data_miner_features_default_rules_configuration() {
  $items = array();
  $items['rules_search_engine_insert_send_email_'] = entity_import('rules_config', '{ "rules_search_engine_insert_send_email_" : {
      "LABEL" : "Search engine insert, send email ",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "rss_feed" : "rss_feed", "engine" : "engine" } }
          }
        },
        { "NOT node_is_published" : { "node" : [ "node" ] } }
      ],
      "DO" : [
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "4" : "4" } },
            "subject" : "A new content inserted",
            "message" : "Hello!\\r\\n\\r\\nA new content inserted by [node:author]. Please, review it at: [node:url]\\r\\n\\r\\nThanks,\\r\\nadmin"
          }
        },
        { "drupal_message" : { "message" : "Thank you for your contribution! An editor will review the submitted content coming soon and if the content is appropriate then it will be enabled." } }
      ]
    }
  }');
  return $items;
}
