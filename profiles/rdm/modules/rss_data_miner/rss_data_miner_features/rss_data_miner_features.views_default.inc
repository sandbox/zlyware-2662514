<?php
/**
 * @file
 * rss_data_miner_features.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function rss_data_miner_features_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'rss_data_miner_content';
  $view->description = 'The main content view for RSS Data Miner.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'RSS Data Miner Content';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Updated/commented date */
  $handler->display->display_options['fields']['last_updated']['id'] = 'last_updated';
  $handler->display->display_options['fields']['last_updated']['table'] = 'node_comment_statistics';
  $handler->display->display_options['fields']['last_updated']['field'] = 'last_updated';
  $handler->display->display_options['fields']['last_updated']['label'] = 'Last updated';
  $handler->display->display_options['fields']['last_updated']['date_format'] = 'long';
  $handler->display->display_options['fields']['last_updated']['second_date_format'] = 'long';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: RSS Feed Children */
  $handler = $view->new_display('block', 'RSS Feed Children', 'rss_feed_children');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Forums from this RSS Feed';
  $handler->display->display_options['display_description'] = 'List child forum nodes on RSS feed node.';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Feed (field_feed) */
  $handler->display->display_options['arguments']['field_feed_nid']['id'] = 'field_feed_nid';
  $handler->display->display_options['arguments']['field_feed_nid']['table'] = 'field_data_field_feed';
  $handler->display->display_options['arguments']['field_feed_nid']['field'] = 'field_feed_nid';
  $handler->display->display_options['arguments']['field_feed_nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_feed_nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['field_feed_nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_feed_nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_feed_nid']['summary_options']['items_per_page'] = '25';

  /* Display: Published engines page */
  $handler = $view->new_display('page', 'Published engines page', 'page_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Search engines';
  $handler->display->display_options['display_description'] = 'This page list the published engines (main menu).';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'engine' => 'engine',
  );
  $handler->display->display_options['path'] = 'engine';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Search engines';
  $handler->display->display_options['menu']['description'] = 'Search engines used by this page.';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Published RSS Feeds page */
  $handler = $view->new_display('page', 'Published RSS Feeds page', 'page_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'RSS Feeds';
  $handler->display->display_options['display_description'] = 'This page list the published feeds (main menu).';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'rss_feed' => 'rss_feed',
  );
  $handler->display->display_options['path'] = 'rss-feed';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'RSS Feeds';
  $handler->display->display_options['menu']['description'] = 'Search engines used by this page.';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Search engine children */
  $handler = $view->new_display('block', 'Search engine children', 'source_engine_children');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'RSS Feeds from this search engine';
  $handler->display->display_options['display_description'] = 'List child RSS Feed nodes on Search engine node.';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Search engine (field_search_engine) */
  $handler->display->display_options['arguments']['field_search_engine_nid']['id'] = 'field_search_engine_nid';
  $handler->display->display_options['arguments']['field_search_engine_nid']['table'] = 'field_data_field_search_engine';
  $handler->display->display_options['arguments']['field_search_engine_nid']['field'] = 'field_search_engine_nid';
  $handler->display->display_options['arguments']['field_search_engine_nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_search_engine_nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['field_search_engine_nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_search_engine_nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_search_engine_nid']['summary_options']['items_per_page'] = '25';
  $export['rss_data_miner_content'] = $view;

  return $export;
}
