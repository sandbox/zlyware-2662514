<?php
/**
 * @file
 * rss_data_miner_features_block.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function rss_data_miner_features_block_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['search-form'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'form',
    'module' => 'search',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'lexus_zymphonies_theme' => array(
        'region' => 'search',
        'status' => 1,
        'theme' => 'lexus_zymphonies_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-help'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'help',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'lexus_zymphonies_theme' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'lexus_zymphonies_theme',
        'weight' => -10,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-management'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'management',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'lexus_zymphonies_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'lexus_zymphonies_theme',
        'weight' => 1,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-navigation'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'navigation',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'lexus_zymphonies_theme' => array(
        'region' => 'footer_second',
        'status' => 1,
        'theme' => 'lexus_zymphonies_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-user-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'user-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'lexus_zymphonies_theme' => array(
        'region' => 'user_menu',
        'status' => 1,
        'theme' => 'lexus_zymphonies_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['user-login'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'login',
    'module' => 'user',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'lexus_zymphonies_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'lexus_zymphonies_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-03c191070c6dfc001236840e2d9a27c4'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '03c191070c6dfc001236840e2d9a27c4',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'lexus_zymphonies_theme' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'lexus_zymphonies_theme',
        'weight' => -7,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-df3b95945214feb988aeb1520ef523a3'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'df3b95945214feb988aeb1520ef523a3',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'lexus_zymphonies_theme' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'lexus_zymphonies_theme',
        'weight' => -8,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
