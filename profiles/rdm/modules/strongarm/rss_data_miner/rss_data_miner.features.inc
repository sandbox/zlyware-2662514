<?php
/**
 * @file
 * rss_data_miner.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function rss_data_miner_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
