<?php
/**
 * @file
 * rss_data_miner.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function rss_data_miner_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'cron_key';
  $strongarm->value = 'O58vx4y-N1_OfHY6I1fVu7j1dURyqNnGxqir2QTCTyQ';
  $export['cron_key'] = $strongarm;

  return $export;
}
