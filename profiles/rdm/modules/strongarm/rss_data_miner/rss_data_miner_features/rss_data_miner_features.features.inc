<?php
/**
 * @file
 * rss_data_miner_features.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function rss_data_miner_features_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
