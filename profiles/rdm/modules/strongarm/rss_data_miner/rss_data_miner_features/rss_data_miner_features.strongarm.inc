<?php
/**
 * @file
 * rss_data_miner_features.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function rss_data_miner_features_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'clean_url';
  $strongarm->value = '1';
  $export['clean_url'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'cron_key';
  $strongarm->value = 'LsAJhVR1intaKLAxoJBCpMW4S7oulI6X-NOYi8rwhoA';
  $export['cron_key'] = $strongarm;

  return $export;
}
