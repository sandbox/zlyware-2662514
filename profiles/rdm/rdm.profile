<?php
/**
 * @file
 * Enables modules and site configuration for installation.
 */

/**
 * Implements hook_install_tasks_alter().
 */
function rdm_install_tasks_alter(&$tasks, $install_state) {
  // Remove the language selection screen.
  $tasks['install_select_locale']['function'] = '_rdm_locale_selection';
} 

/**
 * Local callback function.
 */
function _rdm_locale_selection(&$install_state){
  // Use default language, English.
  $install_state['parameters']['locale'] = 'en';
}

/**
 * Implements hook_install_tasks().
 */
function rdm_install_tasks($install_state) {
  return array(
    'rdm_install_setup_form' => array(
      'display_name' => st('RSS Data Miner'),
      'type' => 'form',
      'run' => INSTALL_TASK_RUN_IF_REACHED,
    ),
  );
}

/**
 * Implements the RSS Data Miner Distribution's configuration form.
 */
function rdm_install_setup_form($form, &$form_state) {
  $form = array();
  
  // RSS Data Miner configuration.
  include_once drupal_get_path('module', 'rss_data_miner') . '/rss_data_miner.admin.inc';
  $form['rss_data_miner_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => st('RSS Data Miner'),
    '#collapsible' => FALSE,
  );
  $form['rss_data_miner_wrapper']['i18n_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use the RSS Data Miner more than one language.'),
    '#default_value' => TRUE,
    '#description' => t('This checkbox will enable the RSS Data Miner Internationalization module.'),
  ); 
  foreach (rss_data_miner_admin_settings($form, $form_state) as $key => $value) {
    if (in_array($key, array('rss_data_miner_keyword'))) {
      $form['rss_data_miner_wrapper'][$key] = $value;
    }
  }
  
  // Opencalais configuration.
  include_once drupal_get_path('module', 'opencalais') . '/opencalais_api.module';
  $form['opencalais_api_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => st('Opencalais'),
    '#collapsible' => FALSE,
  );
  foreach (opencalais_api_admin_settings($form, $form_state) as $key => $value) {
    if (in_array($key, array('opencalais_api_key', 'opencalais_api_server'))) {
      $form['opencalais_api_wrapper'][$key] = $value;
    }
  }
  $form['opencalais_api_wrapper']['opencalais_api_key']['#required'] = TRUE;
  $form['opencalais_api_wrapper']['opencalais_api_server']['#required'] = TRUE;
  
  // Microsoft Translator API configuration.
  include_once drupal_get_path('module', 'microsoft_translator_api') . '/microsoft_translator_api.module';
  $form['microsoft_translator_api_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => st('Microsoft Translator API'),
    '#collapsible' => FALSE,
  );
  foreach (microsoft_translator_api_config_form($form, $form_state) as $key => $value) {
    if (in_array($key, array('microsoft_translator_api_client', 'microsoft_translator_api_secret'))) {
      $form['microsoft_translator_api_wrapper'][$key] = $value;
    }
  }

  $form = system_settings_form($form);
  $form['#submit'][] = 'rdm_install_setup_form_submit';
  return $form;
}

/**
 * Validation of the RSS Data Miner configuration form.
 */
function rdm_install_setup_form_validate($form, &$form_state) {
  // ***** TODO *****
  // Drush cuts the form values which are contains an equal sign.
  // Temporary add the missing = on drush site-install
  if (variable_get('site_name', '') == 'Site-Install') {
    $form['microsoft_translator_api_wrapper']['microsoft_translator_api_secret']['#value'] .= '=';
  }
  microsoft_translator_api_config_form_validate($form['microsoft_translator_api_wrapper'], $form_state);
}

/**
 * Submission of the RSS Data Miner configuration form.
 */
function rdm_install_setup_form_submit($form, &$form_state) {
  // ***** TODO *****
  // Drush cuts the form values which are contains an equal sign.
  // Temporary add the missing = on drush site-install
  if (variable_get('site_name', '') == 'Site-Install') {
    variable_set('microsoft_translator_api_secret', variable_get('microsoft_translator_api_secret') . '=');
  }
  module_enable(array(
    'rss_data_miner_features_block',
  ));
  if ($form['rss_data_miner_wrapper']['i18n_enabled']['#value']) {
    module_enable(array(
      'rss_data_miner_i18n',
    ));
  }
}
